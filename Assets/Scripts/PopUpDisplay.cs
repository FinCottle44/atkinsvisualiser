﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections;


public class PopUpDisplay : MonoBehaviour
{
    public RectTransform popUpDisplayBox;
    public Text popUpDisplayBoxText;
    public float secStart = 0.2f;
    public float secFinish = 3f;
    public CanvasGroup popUpDisplayBoxCanvas;
    public CanvasGroup inputBox;
    public SavePreset save;
    public Text textBoxMessage;
    public GameObject RawImage;
    public CanvasGroup RawImageCanvas;
    // Start is called before the first frame update
    void Start()
    {
        popUpDisplayBox.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void wow(string popUpBoxMessage)
    {
        Debug.Log(popUpBoxMessage);
        popUpDisplayBoxText.GetComponent<Text>().text = popUpBoxMessage;
        StartCoroutine(LateCall());
    }

    IEnumerator LateCall()
    {

        yield return new WaitForSeconds(secStart);
        popUpDisplayBox.gameObject.SetActive(true);
        // popUpDisplayBoxCanvas.DOFade(1f, 3f);
        popUpDisplayBoxCanvas.DOFade(1f, .5f);

        yield return new WaitForSeconds(secFinish);
        popUpDisplayBoxCanvas.DOFade(0f, .5f);
        yield return new WaitForSeconds(.5f);
        popUpDisplayBox.gameObject.SetActive(false);
        //Do Function here...
    }
    // public void inputTextBox(string textBoxMessage,string callBackToo, int MaxLength)

    public IEnumerator textBox()
    {
        Debug.Log("textBox");
        yield return new WaitForSeconds(.2f);
        inputBox.gameObject.SetActive(true);
        RawImage.gameObject.SetActive(true);
        inputBox.DOFade(1f, .5f);
        RawImageCanvas.DOFade(1f, .5f);
    }

    public void textBoxStart()
    {
        StartCoroutine(textBox());
    }

    public void inputTextBox()
    {
        inputBox.DOFade(0f, .5f);
        RawImageCanvas.DOFade(0f, .5f);
        inputBox.gameObject.SetActive(false);
        RawImage.gameObject.SetActive(false);
        Debug.Log("running Input Text Box");
        string inputBoxText = textBoxMessage.GetComponent<Text>().text;
        Debug.Log(inputBoxText);
        save.CreateXml(inputBoxText);
    }

}
