﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongPress : MonoBehaviour
{
    public float TouchStartTime;
    public float Delta;
    public bool Touched;
    public PlaceOnPlane PlaceOnPlane;
    public bool done;
    // Start is called before the first frame update
    void Start()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TouchStartTime = Time.time;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TouchStartTime = Time.time;
        }
    }
    public void LongPressDetector()
    {
        Delta = Time.time - TouchStartTime;
        if (Delta < 0.2)
        {
            PlaceOnPlane.Undo();
            Delta = 0;
        }
        else
        {
            PlaceOnPlane.Clear();
            Delta = 0;
        }

    }
}
