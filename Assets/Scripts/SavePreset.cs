﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Xml;
using System.Xml.Linq;
using DG.Tweening.Plugins.Core.PathCore;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class SavePreset : MonoBehaviour
{
    public GameObject Dialog;
    public GameObject DebugSnack;
    public GameObject OriginCenter;
    public PopUpDisplay MessageBoxSend;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.persistentDataPath);
    }

    private List<PointObject> GatherObjects()
    {
        List<GameObject> points = GameObject.FindGameObjectsWithTag("point").ToList();
        List<PointObject> objects = new List<PointObject>();
        foreach (GameObject point in points)
        {
            string objType = GrabType(point);
            //Debug.log(point.transform.position.ToString());
            //Debug.log(OriginCenter.transform.position.ToString())
            PointObject obj = new PointObject(objType, point.transform.position - OriginCenter.transform.position, point.transform.rotation);
            objects.Add(obj);
        }
        return objects;
    }

    //Checks if directory is alrady made, if not, makes it
    private void MakeDirectory()
    {
        string path = Application.persistentDataPath + "/Presets";
        if (!Directory.Exists(path))
        {
            System.IO.Directory.CreateDirectory(path);
        }
        else
        {
            print("Directory already exists!");
        }
    }

    public class PointObject
    {
        public PointObject(string s, Vector3 v, Quaternion q)
        {
            ObjectType = s;
            Position = v;
            Rotation = q;
        }
        public string ObjectType { get; set; }
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }
    }

    public void CreateXml(string filename)
    {
        MakeDirectory(); //create directory to save items

        List<PointObject> objects = GatherObjects();
        XDocument positions = new XDocument(
            new XComment("Positions of preset values"),
            new XElement("objects")
        );

        int x = 1;
        foreach (PointObject obj in objects)
        {
            //Example: <object2 Position="(0.1, 0.2, 0.3)" Rotation="(0.3, 0.2, 0.1)">3</object2>
            XElement el = new XElement("object" + x, new XAttribute("Position", obj.Position.ToString()), new XAttribute("Rotation", obj.Rotation.ToString()), obj.ObjectType);
            positions.Root.Add(el); //adds el as child of the "objects" element defined above (root of positions file)
            Debug.Log("el value = " + el.Value + " / el pos = " + obj.Position + " / el rot = " + obj.Rotation + "/ el type = " + obj.ObjectType);
            x = x + 1;
        }

        string saveString = Application.persistentDataPath + "/Presets/@" + filename + ".xml"; //@ allows for removal of path in loadpreset
        positions.Save(saveString);

        //remove buggy XML tag - doesnt work at the mo
        XDocument doc = new XDocument();
        doc = XDocument.Load(saveString);
        foreach (XNode node in doc.DescendantNodes())
        {
            if (node.NodeType == XmlNodeType.XmlDeclaration)
            {
                node.Remove();
            }
        }

        doc.Save(saveString);
        // AndroidPlugin.ShowToast("Preset successfully saved as '" + filename + "'", true);
        MessageBoxSend.wow("Preset successfully saved as '" + filename + "'");

    }

    private string GrabType(GameObject go)
    {
        string output;
        if (go.name.Contains("Cone"))
        {
            output = "cone";
        }
        else if (go.name.Contains("Barrier"))
        {
            output = "barrier";
        }
        else if (go.name.Contains("marker"))
        {
            output = "marker";
        }
        else if (go.name.Contains("ring"))
        {
            output = "ring";
        }
        else
        {
            output = "null";
        }

        return output;
    }

    public void SaveName()
    {
        // AndroidPlugin.ShowSingleLineTextDialog("Enter filename", "Please enter a name for this preset:","filename", 15, "SavePreset", "CreateXml", "Save");
        MessageBoxSend.textBoxStart();
        OriginCenter = GameObject.FindGameObjectWithTag("originCenter");
        Debug.Log("SaveName");
    }
}
