﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class LoadPreset : MonoBehaviour
{
    public PlaceOnPlane ArController;

    public string PresetPath;
    public List<GameObject> PresetButtons;
    public List<GameObject> PresetButtonTexts;
    public GameObject DebugSnack;
    public GameObject Panel;

    public GameObject marker;
    public GameObject cone;
    public GameObject barrier;
    public GameObject ring;
    public GameObject originCenter;

    // Start is called before the first frame update
    void Start()
    {
        PresetPath = Application.persistentDataPath + "/Presets";
        Panel = GameObject.Find("Panel");
        ArController = FindObjectOfType<PlaceOnPlane>();
    }

    private void GrabButtonsAndText()
    {
        Debug.Log("grabbing buttons");
        Debug.Log(PresetButtons);
        //PresetButtons = GameObject.FindGameObjectsWithTag("presetButton").ToList();
        Debug.Log(PresetButtons.Count);
        PresetButtons = new List<GameObject>();
        PresetButtons.Clear();
        foreach (Transform child in Panel.transform)
        {
            if (child.tag == "presetButton")
            {
                Debug.Log("Hi");
                PresetButtons.Add(child.gameObject);
            }
        }
        PresetButtonTexts = GameObject.FindGameObjectsWithTag("presetName").ToList();
    }

    public void PopulateText()
    {
        GrabButtonsAndText();
        List<string> files = ListPresets();
        int amountOfFiles = files.Count();
        GameObject numberOfFilesText = GameObject.FindGameObjectWithTag("PresetsTitle");
        numberOfFilesText.GetComponent<Text>().text = "Number of Presets: " + amountOfFiles;
        int k = 0;

        foreach (GameObject btn in PresetButtons)
        {
            btn.SetActive(true);//quickly so we can make judgement on whether amount needs ot be updated

            if (PresetButtons.IndexOf(btn) > amountOfFiles - 1)
            {
                btn.SetActive(false);
            }
        }
        GrabButtonsAndText(); //grab again without deactivated ones
        int i = 0;
        foreach (string f in files)
        {
            PresetButtonTexts[i].GetComponent<Text>().text = f;
            i += 1;
        }
    }

    private List<string> ListPresets()
    {
        List<string> filenames = new List<string>();
        string[] filepaths = Directory.GetFiles(PresetPath);
        foreach (string f in filepaths)
        {
            //remove path
            int pos = f.IndexOf("@");
            string filenameNoPath = f.Remove(0, pos); //removes all characters up to the @ in the filename (the path)
            string filenameNoAt = filenameNoPath.Replace("@", ""); //removes the @ symbol
            filenames.Add(filenameNoAt);
        }
        return filenames;
    }

    public void LoadSelectedPreset()
    {
        string selectedPresetName = gameObject.transform.Find("presetNameText").GetComponent<Text>().text;
        Debug.Log("selected preset name text: " + selectedPresetName);
        string file = PresetPath + "/@" + selectedPresetName;
        XDocument doc = new XDocument();
        doc = XDocument.Load(file);

        GameObject parent = new GameObject();
        foreach (XElement el in doc.Root.Elements())
        {
            string type = el.Value;

            Vector3 pos = VectorSplit(el.Attribute("Position").ToString());
            Quaternion rot = QuaternionSplit(el.Attribute("Rotation").ToString());
            SavePreset.PointObject obj = new SavePreset.PointObject(type, pos, rot); //create object with these tings

            GameObject item = null;
            originCenter = GameObject.FindGameObjectWithTag("originCenter");

            switch (obj.ObjectType)
            {
                case "cone":
                    item = Instantiate(cone, obj.Position + originCenter.transform.position, obj.Rotation);
                    break;
                case "Marker":
                    item = Instantiate(marker, obj.Position + originCenter.transform.position, obj.Rotation);
                    break;
                case "barrier":
                    item = Instantiate(barrier, obj.Position + originCenter.transform.position, obj.Rotation);
                    break;
                case "ring":
                    item = Instantiate(ring, obj.Position + originCenter.transform.position, obj.Rotation);
                    break;
            }
            item.SetActive(false);
            item.transform.parent = parent.transform;

        }
        Debug.Log("PARENT CHILD COUNT = " + parent.transform.childCount);
        ArController.SpawnObject(parent);
    }

    private Vector3 VectorSplit(string str)
    {
        //passed in like this: '"Position="(0.2,-0.7, 0.1)"'
        //remove position
        int pos = str.IndexOf("=");
        string sVector = str.Remove(0, pos + 1);// ' "(x,y,z)" '

        // Remove the quotes
        if (sVector.StartsWith('"'.ToString()) && sVector.EndsWith('"'.ToString()))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));
        return result;
    }

    private Quaternion QuaternionSplit(string str)
    {
        int pos = str.IndexOf("=");
        string sQuat = str.Remove(0, pos + 1);

        // Remove the quotes
        if (sQuat.StartsWith('"'.ToString()) && sQuat.EndsWith('"'.ToString()))
        {
            sQuat = sQuat.Substring(1, sQuat.Length - 2);
        }

        // Remove the parentheses
        if (sQuat.StartsWith("(") && sQuat.EndsWith(")"))
        {
            sQuat = sQuat.Substring(1, sQuat.Length - 2);
        }

        //split items
        string[] sArray = sQuat.Split(',');

        //store as Quat
        Quaternion result = new Quaternion(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]),
            float.Parse(sArray[3]));
        return result;
    }
}
