﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using Sirenix.OdinInspector;

public class sliderController : MonoBehaviour
{
    private Vector3 HiddenPos;
    public Vector3 panelPos, targetPos;
    public RectTransform panel,panelTarget;
    public float openPos, closePos;
    public float openPosMenu, closePosMenu;
    public bool toggle;
    public bool toggleMenu;
    public RectTransform panelMenu;


    // Start is called before the first frame update
    void Start()
    {
       // panelPos = panel.anchoredPosition;
        //targetPos = panelTarget.anchoredPosition;
       // HiddenPos = new Vector3(0f, 0f, 0f);

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Show()
    {
        panel.DOAnchorPosX(openPos, 0.5f);
    }
    public void Hide()
    {
        panel.DOAnchorPosX(closePos, 0.3f).OnComplete(() =>
        {

        });
    }
    public void Toggle()
    {
        if (toggle == false)
        {
            panel.DOAnchorPosX(openPos, 0.5f);
            toggle = !toggle;
        }
        else
        {
            panel.DOAnchorPosX(closePos, 0.5f).OnComplete(() =>
            {

            });
            toggle = !toggle;
        }

    }

    public void ToggleMenu()
    {
        Debug.Log("toggle menu CALLED: ");
        if (toggleMenu == false)
        {
            panelMenu.DOAnchorPosX(openPosMenu, 0.5f);
            toggleMenu = !toggleMenu;
        }
        else
        {
            panelMenu.DOAnchorPosX(closePosMenu, 0.5f).OnComplete(() =>
           {

           });
            toggleMenu = !toggleMenu;
        }
    }

}
