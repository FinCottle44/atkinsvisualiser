﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class menuScript : MonoBehaviour
{
    public GameObject btnGetStarted;

    public void ChangeScene()
    {
        SceneManager.LoadScene("BuildScene");
    }
    public void ChangePresetScene()
    {
        SceneManager.LoadScene("PresetScene");
    }
    public void BackButton()
    {
        SceneManager.LoadScene("menuScene");
    }
    public void getStarted()
    {
        btnGetStarted.SetActive(true);
    }
}
