﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;
using DG.Tweening;
/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
///
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARSessionOrigin))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    /// <summary>s
    /// The prefab to instantiate on touch.
    /// </summary>
    // public List<GameObject> Objects;
    public Stack<GameObject> Objects = new Stack<GameObject>();
    public Stack<GameObject> ClearedStack = new Stack<GameObject>();
    public GameObject PlacedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }
    public GameObject SelectedPrefab;
    public GameObject ConePrefab;
    public GameObject RingPrefab;
    public GameObject BarrierPrefab;
    public int SelectionValue;
    public int CurrentSelectionValue;
    public bool IgnoreTouchVar;
    public bool CenterPointPlace;
    public GameObject CenterPointPrefab;
    public GameObject LockPrefab;


    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }
    void Awake()
    {
        m_SessionOrigin = GetComponent<ARSessionOrigin>();
    }

    void Update()
    {
    
        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);

        if (IgnoreTouchVar)
        {
            IgnoreTouchVar = false;
            if (CurrentSelectionValue != SelectionValue) // use return
            {
                GetSelectedPrefab(SelectionValue);
                m_PlacedPrefab = SelectedPrefab;
                GameObject TempSpawnedObject = Instantiate(m_PlacedPrefab, spawnedObject.transform.position, spawnedObject.transform.rotation);
                Destroy(spawnedObject);
                spawnedObject = TempSpawnedObject;
                CurrentSelectionValue = SelectionValue;
            }
        }
        else
        {
            if (m_SessionOrigin.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon) && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                var hitPose = s_Hits[0].pose;
                if (CenterPointPlace == true)
                {
                    m_PlacedPrefab = CenterPointPrefab;
                    if (spawnedObject == null)
                    {
                        spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                        ShowLock();
                        spawnedObject.tag = "originCenter";
                    }
                    else
                    {
                        spawnedObject.transform.position = hitPose.position;
                    }
                    return;
                }
                // Raycast hits are sorted by distance, so the first one
                // will be the closest hit.
                GetSelectedPrefab(SelectionValue);
                m_PlacedPrefab = SelectedPrefab;
                if (spawnedObject == null)
                {
                    spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                    CurrentSelectionValue = SelectionValue;
                    ShowLock();
                    spawnedObject.tag = "point";
                }
                else
                {
                    spawnedObject.transform.position = hitPose.position;
                }
            }
        }

    }


    // Selection
    public void GetSelectedPrefab(int value)
    {

        switch (value)
        {
            case 1:
                SelectedPrefab = ConePrefab;
                break;
            case 2:
                SelectedPrefab = RingPrefab;
                break;
            case 3:
                SelectedPrefab = BarrierPrefab;
                break;
            case -1:
                SelectedPrefab = null;
                break;
            default:
                SelectedPrefab = RingPrefab;
                break;

        }
        
    }
    public void SelectCone()
    {
        SelectionValue = 1;
        IgnoreTouch();
    }
    public void SelectRing()
    {
        SelectionValue = 2;
        IgnoreTouch();
    }
    public void SelectBarrier()
    {
        SelectionValue = 3;
        IgnoreTouch();
    }
    public void SelectNull()
    {
        SelectionValue = -1;//used to ignore touches on the UI, by placing a null item
    }
    // End of Selection
    // Functions
    public void SpawnObject(GameObject item)
    {
        if (Input.touchCount == 0)
            return;
        //var hitPose = s_Hits[0].pose;
        //var anchor = Instantiate(null, hitPose.position, hitPose.rotation);
        //item.transform.parent = ;
        //var anchor = hit.Trackable.CreateAnchor(hit.Pose);
        //item.transform.parent = hitPose.transform;

        //GameObject TempSpawnedObject = Instantiate(m_PlacedPrefab, spawnedObject.transform.position, spawnedObject.transform.rotation);
        //item.transform.localPosition = Vector3.zero;
        //item.transform.localRotation = new Quaternion(0f,0f,0f,0f);
        //item.transform.position = anchor.transform.position;

        Debug.Log("item position: " + item.transform.position);

        Transform[] children = item.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in children)
        {
            if (child.parent == item.transform) //not including children of children
            {
                Debug.Log(child.name);
                Debug.Log("child pos: " + child.transform.position);
                //child.Rotate(0, k_ModelRotation, 0, Space.Self);
                GameObject go = child.gameObject;
                go.tag = "point";
                //displacement.Display(go.gameObject);
                go.SetActive(true);
                Objects.Push(go);
            }
        }

        //undoAnchor.Push(anchor);
    }
    public void ShowLock()
    {
        LockPrefab.SetActive(true);
    }
    public void HideLock()
    {
        LockPrefab.SetActive(false);
    }
    public void Undo()
    {
        IgnoreTouch();
        Debug.Log(Objects.Count + "Objects Count");
        if (Objects.Count == 0)
        {

            Debug.Log(ClearedStack.Count + "cleared count");
            int var = ClearedStack.Count; // allows for cleared gameObjects to be undone
            for (int i = 0; i < var; i++)
            {
                GameObject temp = ClearedStack.Pop();
                temp.SetActive(true);
                Objects.Push(temp);
            }
        }
        else
        {
            GameObject temp = Objects.Pop();
            Destroy(temp);
        }
    }
    public void Clear()
    {
        IgnoreTouch();
        Debug.Log(Objects.Count + "object count at clear");
        int var = Objects.Count;
        for (int i = 0; i <= var; i++)
        {
            Debug.Log(i + "i count");
            GameObject temp = Objects.Pop();
            temp.SetActive(false);
            ClearedStack.Push(temp);
        }


    }
    public void IgnoreTouch() // Jack: Ignore next touch
    {
        IgnoreTouchVar = true;
    }
    public void LockIn()
    {
        IgnoreTouch();
        HideLock();
        if (CenterPointPlace == true)
        {
            spawnedObject = null;
            SelectedPrefab = ConePrefab;
            CenterPointPlace = false;
        }
        else
        {
            Objects.Push(spawnedObject);
            spawnedObject = null;
            SelectedPrefab = PlacedPrefab;
        }

        
    }
    public void DisplacementDisplay()
    {
    }
    // End of Functions
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARSessionOrigin m_SessionOrigin;
}
