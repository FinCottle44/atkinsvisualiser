﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPrefab : MonoBehaviour
{
    private int TapCount;
    private float NewTime;
    private bool rotate;
    private bool AlreadyRotating;
    private PlaceOnPlane pop;
    private void Start()
    {
        pop = FindObjectOfType<PlaceOnPlane>();
    }
    void LateUpdate()
    {
        if (Input.touchCount == 0) { rotate = false; }
        float pinchAmount = 0;
        Quaternion desiredRotation = transform.rotation;
       
        DetectTouchMovement.Calculate();

        if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
        {
            // rotate
            Debug.Log("Attempting rotation");
            Vector3 rotationDeg = Vector3.zero;
            rotationDeg.y = -DetectTouchMovement.turnAngleDelta;
            desiredRotation *= Quaternion.Euler(rotationDeg);
        }

        if (Input.touchCount > 1)
        {
            pop.IgnoreTouch();
            Touch touch = Input.GetTouch(0);
            Debug.Log("touch is: " + touch.phase);
            Debug.Log("camera is " + Camera.main.name);
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;
            Debug.Log("sent raycast");
            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.Log("hit this object: " + hit.collider.name);
                Transform hitTrans = hit.collider.transform;
                if (hitTrans == transform || hitTrans.parent == transform || hitTrans.parent.parent == transform)
                {
                    Debug.Log("setting rotate to true");
                    rotate = true;
                }
            }
        }

        if (rotate)
        {
            Debug.Log("applying rotation");
            transform.rotation = desiredRotation;
        }

        //transform.position += Vector3.forward * pinchAmount;


        //if (Input.touchCount == 1)
        //{
        //    Touch touch = Input.GetTouch(0);

        //    if (touch.phase == TouchPhase.Ended)
        //    {
        //        TapCount += 1;
        //    }

        //    if (TapCount == 1)
        //    {
        //        NewTime = Time.time + MaxDubTapTime;
        //    }
        //    else if (TapCount == 2 && Time.time <= NewTime)
        //    {

        //        //Whatever you want after a dubble tap    


        //        gameObject.SetActive(false);

        //        TapCount = 0;
        //    }
        //}
    }
}
